from django.contrib import auth
from django.shortcuts import render, redirect

from .forms import LoginForm, UserRegistrationForm


# Create your views here.
def login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = auth.authenticate(username=cd['username'], password=cd['password'])
            if user is not None:
                # gdzie należy wywalić błąd:
                if user.is_active:
                    auth.login(request, user)
                    return redirect('home')

                else:
                    # return HttpResponse('Konto jest zablokowane')
                    return render(request, 'accounts/login.html', {'error': 'Konto jest zablokowane.'})
            else:
                return render(request, 'accounts/login.html', {'error': 'Podano nieprawidłowy login lub hasło'})
    else:
        form = LoginForm()
    return render(request, 'accounts/login.html', {'form': form})


def signup(request):
    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)
        if user_form.is_valid():
            new_user = user_form.save(commit=False)
            new_user.first_name = user_form.cleaned_data['first_name']
            new_user.last_name = user_form.cleaned_data['last_name']
            new_user.email = user_form.cleaned_data['email']
            new_user.set_password(user_form.cleaned_data['password1'])
            new_user.save()

            user = auth.authenticate(username=user_form.cleaned_data['username'],
                                     password=user_form.cleaned_data['password1'])
            auth.login(request, user)
            return redirect('home')
        else:
            return render(request, 'accounts/signup.html', {'user_form': user_form})
    else:
        user_form = UserRegistrationForm()
        return render(request, 'accounts/signup.html', {'user_form': user_form})


def logout(request):
    if request.method == 'POST':
        auth.logout(request)
        return redirect('home')  # grafik/urls.py


