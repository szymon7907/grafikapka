from django.contrib import admin

from .models import Animal, AnimalStatus, AnimalType

admin.site.register(AnimalStatus)
admin.site.register(AnimalType)


# Register your models here.
@admin.register(Animal)
class AnimalAdmin(admin.ModelAdmin):
    list_display = ('name', 'type', 'owner', 'condition')
    list_filter = ('name', 'type', 'condition')
    raw_id_fields = ('owner', 'type', 'condition')
