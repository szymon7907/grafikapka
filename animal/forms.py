from django import forms

from .models import Animal


class CreateForm(forms.ModelForm):
    class Meta:
        model = Animal
        fields = ('name', 'type', 'image', 'condition')
        labels = {'name': 'Nazwa zwierzaka', 'type': 'Rodzaj zwierzaka', 'image': 'Zdjęcie zwierzaka',
                  'condition': 'Status'}
