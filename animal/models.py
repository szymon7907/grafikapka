from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class AnimalType(models.Model):
    type = models.CharField(max_length=50)

    def __str__(self):
        return self.type


class AnimalStatus(models.Model):
    status = models.CharField(max_length=50)

    def __str__(self):
        return self.status


class Animal(models.Model):
    name = models.CharField(max_length=50)
    type = models.ForeignKey(AnimalType, on_delete=models.CASCADE, related_name="animal_animaltype")
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    condition = models.ForeignKey(AnimalStatus, on_delete=models.CASCADE, related_name="animal_animalstatus")
    image = models.ImageField(upload_to='images/')

    def __str__(self):
        return self.name
