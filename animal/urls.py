from django.urls import path

from . import views

urlpatterns = [
    path('', views.dashboard, name='dashboard'),
    path('create/', views.add, name='add'),
    path('<int:animal_id>/delete', views.delete, name='delete'),
    path('<int:animal_id>/', views.edit, name='edit'),
]
