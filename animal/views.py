from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect

from .forms import CreateForm
from .models import Animal


def home(request):
    return render(request, 'animal/home.html')


@login_required(login_url="/accounts/login")
def dashboard(request):
    animals = Animal.objects.filter(owner__id=request.user.id)
    return render(request, 'animal/dashboard.html', {'animals': animals})


@login_required(login_url="/accounts/login")
def delete(request, animal_id):
    animal = get_object_or_404(Animal, pk=animal_id, owner__id=request.user.id)
    message = f'Usunięto {animal.name}.'
    animal.delete()
    animals = Animal.objects.filter(owner__id=request.user.id)
    return render(request, 'animal/dashboard.html', {'message': message, 'animals': animals})


@login_required(login_url="/accounts/login")
def edit(request, animal_id):
    animal = get_object_or_404(Animal, pk=animal_id, owner__id=request.user.id)
    form = CreateForm(instance=animal)
    form.buttonName = 'Zaktualizuj'
    if request.method == 'POST':
        form = CreateForm(request.POST, request.FILES)
        if form.is_valid():
            animal.name = form.cleaned_data['name']
            animal.type = form.cleaned_data['type']
            animal.image = form.cleaned_data['image']
            animal.condition = form.cleaned_data['condition']
            animal.owner = request.user
            animal.save()
            return redirect('dashboard')
        else:
            return render(request, 'animal/add.html', {'error': 'Wszystkie pola są wymagane.', 'form': form})
    else:
        return render(request, 'animal/add.html', {'form': form})


@login_required(login_url="/accounts/login")
def add(request):
    form = CreateForm()
    if request.method == 'POST':
        form = CreateForm(request.POST, request.FILES)
        if form.is_valid():
            animal = Animal()
            animal.name = form.cleaned_data['name']
            animal.type = form.cleaned_data['type']
            animal.image = form.cleaned_data['image']
            animal.condition = form.cleaned_data['condition']
            animal.owner = request.user
            animal.save()
            return redirect('dashboard')
        else:
            return render(request, 'animal/add.html', {'error': 'Wszystkie pola są wymagane.', 'form': form})
    else:
        return render(request, 'animal/add.html', {'form': form})
