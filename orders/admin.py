from django.contrib import admin

from .models import OrderStatus, Order, ServiceType

# Register your models here.
admin.site.register(OrderStatus)
admin.site.register(ServiceType)

@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    raw_id_fields = ('service_id', 'status_id', 'owner_id', 'animal_id',)



