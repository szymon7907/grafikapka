from django import forms

from .models import ServiceType, Order


class DateForm(forms.Form):
    date = forms.DateTimeField(
        input_formats=['%d/%m/%Y %H:%M'],
        widget=forms.DateTimeInput(attrs={
            'class': 'form-control datetimepicker-input',
            'data-target': '#id_date_ordered'
        })
    )


class CreateServiceTypeForm(forms.ModelForm):
    class Meta:
        model = ServiceType
        fields = ['animal_type']
        labels = {'animal_type': 'Rodzaj zwierzaka'}


class CreateOrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ['service_id', 'animal_id', 'status_id', 'cost', 'date_ordered', 'length']
