# Generated by Django 2.2.3 on 2019-07-19 13:01

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('animal', '0005_auto_20190718_1643'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='OrderStatus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='ServiceType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('cost', models.DecimalField(decimal_places=2, default=0, max_digits=4)),
                ('duration', models.DurationField()),
                ('animal_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='animal.AnimalType')),
            ],
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cost', models.DecimalField(decimal_places=2, max_digits=4)),
                ('date_ordered', models.DateTimeField()),
                ('length', models.DurationField()),
                ('animal_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='animal.Animal')),
                ('owner_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('service_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='orders.ServiceType')),
                ('status_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='orders.OrderStatus')),
            ],
        ),
    ]
