from django.contrib.auth.models import User
from django.db import models

from animal.models import Animal, AnimalType


# Create your models here.
class OrderStatus(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class ServiceType(models.Model):
    name = models.CharField(max_length=255)
    animal_type = models.ForeignKey(AnimalType, on_delete=models.CASCADE)
    cost = models.DecimalField(decimal_places=2, max_digits=4, default=0)
    duration = models.DurationField()

    def __str__(self):
        return self.name


class Order(models.Model):
    service_id = models.ForeignKey(ServiceType, on_delete=models.CASCADE)
    status_id = models.ForeignKey(OrderStatus, on_delete=models.CASCADE)
    owner_id = models.ForeignKey(User, on_delete=models.CASCADE)
    animal_id = models.ForeignKey(Animal, on_delete=models.CASCADE)
    cost = models.DecimalField(decimal_places=2, max_digits=4)
    date_ordered = models.DateTimeField()
    length = models.DurationField()

    def __str__(self):
        return f'Zamówienie {self.id} złożone przez {self.owner_id.username}'
