from django.urls import path

from . import views

urlpatterns = [
    path('', views.orders, name='orders'),
    path('add/', views.add_order, name='add_order'),
    path('getservices/<int:pk>', views.get_services, name='get_services'),
    path('getanimals/<int:pk>', views.get_animals, name='get_animals'),

]
