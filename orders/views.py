from datetime import timedelta

from django.contrib.auth.decorators import login_required
from django.core.serializers import serialize
from django.http import JsonResponse
from django.shortcuts import render
from django.utils import timezone

from animal.models import Animal
from .forms import CreateServiceTypeForm, CreateOrderForm
from .models import ServiceType, Order


# Create your views here.
@login_required(login_url="/accounts/login")
def orders(request):
    ordered = Order.objects.filter(owner_id=request.user)
    return render(request, 'orders/orders.html', {'orders': ordered})


@login_required(login_url="/accounts/login")
def add_order(request):
    form = CreateServiceTypeForm()
    order = CreateOrderForm()
    if request.method == 'POST':
        created_order = CreateOrderForm(request.POST)
        if created_order.is_valid():
            new_order = created_order.save(commit=False)
            new_order.service_id = created_order.cleaned_data['service_id']
            new_order.owner_id = request.user
            new_order.cost = created_order.cleaned_data['cost']
            if created_order.cleaned_data['date_ordered'] < timezone.now() + timedelta(days=3):
                return render(request, 'orders/add_order.html',
                              {'form': form, 'order': created_order,
                               'errors': "Zlecenia należy składać z minimum 3 dniowym wyprzedzeniem."})
            new_order.date_ordered = created_order.cleaned_data['date_ordered']
            new_order.length = created_order.cleaned_data['length']
            new_order.animal_id = created_order.cleaned_data['animal_id']
            new_order.save()
            ordered = Order.objects.filter(owner_id=request.user)
            return render(request, 'orders/orders.html', {'orders': ordered})
        else:
            errors = 'Nie wszystkie pola są wypełnione'
            return render(request, 'orders/add_order.html', {'form': form, 'order': created_order, 'errors': errors})
    else:
        return render(request, 'orders/add_order.html', {'form': form, 'order': order})


@login_required(login_url="/accounts/login")
def get_services(request, pk):
    services = ServiceType.objects.filter(animal_type__id=pk)
    services_serialized = serialize('json', services)
    return JsonResponse(services_serialized, safe=False)


@login_required(login_url="/accounts/login")
def get_animals(request, pk):
    animals = Animal.objects.filter(owner__id=request.user.id, type__id=pk)
    animals_serialized = serialize('json', animals)
    return JsonResponse(animals_serialized, safe=False)
